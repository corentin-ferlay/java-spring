package com.tse.info.kanban.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Developer {
    private @Id @GeneratedValue Long id;
    private String lastname;
    private String firstname;
    private String password;
    private String email;
    private LocalDate startContract;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "developers", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("developers")
    private Set<Task> tasks;

    public Developer() {
        this.tasks = new HashSet<>();
    }

    public Developer(String firstname, String lastname, String email, String password) {
        this.tasks = new HashSet<>();
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.startContract = LocalDate.now();
    }

}
