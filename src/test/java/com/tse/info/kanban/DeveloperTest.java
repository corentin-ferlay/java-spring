package com.tse.info.kanban;

import com.tse.info.kanban.service.DeveloperService;
import lombok.extern.slf4j.Slf4j;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class DeveloperTest {

    @Autowired
    DeveloperService developerService;

    @Test
    public void findAllDevelopers() {
        assertEquals(4, this.developerService.findAllDevelopers().size());
    }
}
