package com.tse.info.kanban.service.impl;

import com.tse.info.kanban.repository.DeveloperRepository;
import com.tse.info.kanban.domain.Developer;
import com.tse.info.kanban.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    private DeveloperRepository developerRepository;

    @Override
    public List<Developer> findAllDevelopers()  {
        return this.developerRepository.findAll();
    }
}
