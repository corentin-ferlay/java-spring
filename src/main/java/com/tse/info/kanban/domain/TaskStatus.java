package com.tse.info.kanban.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class TaskStatus {
    private @Id @GeneratedValue Long id;

    private String label;

    private Integer statusOrder;

    @OneToMany(mappedBy = "status")
    private Set<Task> tasks;

    public TaskStatus(String label, Integer statusOrder) {
        this.label = label;
        this.statusOrder = statusOrder;
    }
}
