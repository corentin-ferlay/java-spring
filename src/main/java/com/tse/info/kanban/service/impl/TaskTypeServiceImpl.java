package com.tse.info.kanban.service.impl;

import com.tse.info.kanban.domain.TaskType;
import com.tse.info.kanban.repository.TaskTypeRepository;
import com.tse.info.kanban.service.TaskTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskTypeServiceImpl implements TaskTypeService {

    @Autowired
    private TaskTypeRepository taskTypeRepository;

    @Override
    public List<TaskType> findAllTaskTypes() {
        return this.taskTypeRepository.findAll();
    }
}
