package com.tse.info.kanban.service.impl;

import com.tse.info.kanban.domain.ChangeLog;
import com.tse.info.kanban.repository.ChangeLogRepository;
import com.tse.info.kanban.service.ChangeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChangeLogServiceImpl implements ChangeLogService {

    @Autowired
    private ChangeLogRepository changeLogRepository;

    @Override
    public List<ChangeLog> findAllChangeLogs() {
        return this.changeLogRepository.findAll();
    }
}
