package com.tse.info.kanban.service.impl;

import com.tse.info.kanban.domain.TaskStatus;
import com.tse.info.kanban.repository.TaskStatusRepository;
import com.tse.info.kanban.service.TaskStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskStatusServiceImpl implements TaskStatusService {

    @Autowired
    private TaskStatusRepository taskStatusRepository;

    @Override
    public List<TaskStatus> findAllTaskStatuses() {
        return this.taskStatusRepository.findAll();
    }

    @Override
    public TaskStatus findByStatusOrder(Integer statusOrder) {
        return this.taskStatusRepository.findByStatusOrder(statusOrder).get(0);
    }
}
