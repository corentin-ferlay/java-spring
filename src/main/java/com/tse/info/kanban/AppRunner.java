package com.tse.info.kanban;

import com.tse.info.kanban.domain.Developer;
import com.tse.info.kanban.domain.Task;
import com.tse.info.kanban.domain.TaskStatus;
import com.tse.info.kanban.domain.TaskType;
import com.tse.info.kanban.repository.DeveloperRepository;
import com.tse.info.kanban.repository.TaskRepository;
import com.tse.info.kanban.repository.TaskStatusRepository;
import com.tse.info.kanban.repository.TaskTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AppRunner implements CommandLineRunner {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    DeveloperRepository developerRepository;

    @Autowired
    TaskStatusRepository taskStatusRepository;

    @Autowired
    TaskTypeRepository taskTypeRepository;

    @Override
    public void run(String... args) throws Exception {
        developerRepository.save(new Developer("Corentin", "Ferlay", "corentinferlay@yahoo.fr", "test"));
        developerRepository.save(new Developer("Julien", "Giov", "julien.inspire@gmail.com", "test"));
        developerRepository.save(new Developer("Thomas", "Casa", "thomas.casa@gmail.com", "test"));
        developerRepository.save(new Developer("Dorian", "Pill", "dorian.pill@gmail.com", "test"));

        TaskStatus statusTodo = new TaskStatus("TODO", 1);
        TaskStatus statusOngoing = new TaskStatus("ONGOING", 2);
        TaskStatus statusDone = new TaskStatus("DONE", 3);
        taskStatusRepository.save(statusTodo);
        taskStatusRepository.save(statusOngoing);
        taskStatusRepository.save(statusDone);

        TaskType typeBug = new TaskType("Bug");
        TaskType typeFeature = new TaskType("Feature");
        TaskType typeCore = new TaskType("Core");
        taskTypeRepository.save(typeBug);
        taskTypeRepository.save(typeFeature);
        taskTypeRepository.save(typeCore);

        taskRepository.save(new Task("Add controllers", 3, typeBug, statusTodo));
        taskRepository.save(new Task("Add unit tests", 10, typeCore, statusTodo));

        log.info("Repository initialized");
    }
}
