package com.tse.info.kanban;

import com.tse.info.kanban.domain.Developer;
import com.tse.info.kanban.domain.Task;
import com.tse.info.kanban.domain.TaskStatus;
import com.tse.info.kanban.domain.TaskType;
import com.tse.info.kanban.repository.TaskRepository;
import com.tse.info.kanban.repository.TaskStatusRepository;
import com.tse.info.kanban.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class TaskTest {

    @Autowired
    TaskService taskService;

    @Autowired
    TaskRepository taskRepository;

    Developer dev = new Developer("Corentin", "Ferlay", "corentinferlay@yahoo.fr", "test");
    Task task1 = new Task("Create unit test", 10, new TaskType("TODO"), new TaskStatus("TODO", 1));

    @Test
    public void addDeveloper() {
        task1.addDeveloper(dev);
        assertEquals(1, dev.getTasks().size());
        assertEquals(1, task1.getDevelopers().size());
    }

    @Test
    public void getAndSaveTask() {
        assertEquals(2, this.taskRepository.findAll().size());
        this.taskRepository.save(new Task());
        assertEquals(3, this.taskRepository.findAll().size());
        assertEquals(3, this.taskService.findAllTasks().size());
    }

    @Test
    public void findTask() {
        Task testTask = new Task();
        this.taskRepository.save(testTask);
        assertEquals(testTask, this.taskService.findTask(testTask.getId()));
    }

    @Test
    public void moveTask() {
        this.taskService.moveRightTask(task1);
        assertEquals(2, task1.getStatus().getStatusOrder());
        this.taskService.moveLeftTask(task1);
        assertEquals(1, task1.getStatus().getStatusOrder());
    }
}
