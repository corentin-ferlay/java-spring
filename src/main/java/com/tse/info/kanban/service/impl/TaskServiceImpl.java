package com.tse.info.kanban.service.impl;

import com.tse.info.kanban.domain.Task;
import com.tse.info.kanban.domain.TaskStatus;
import com.tse.info.kanban.repository.TaskRepository;
import com.tse.info.kanban.service.TaskService;
import com.tse.info.kanban.service.TaskStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskStatusService taskStatusService;

    @Override
    public List<Task> findAllTasks() {
        return this.taskRepository.findAll();
    }

    @Override
    public Task findTask(Long id) {
        return this.taskRepository.findById(id).orElse(null);
    }

    @Override
    public Task moveRightTask(Task task) {
        Integer currentOrder = task.getStatus().getStatusOrder();
        TaskStatus nextStatus = this.taskStatusService.findByStatusOrder(currentOrder + 1);
        if(nextStatus != null) {
            task.setStatus(nextStatus);
        }
        return task;
    }

    @Override
    public Task moveLeftTask(Task task) {
        Integer currentOrder = task.getStatus().getStatusOrder();
        if(currentOrder > 1) {
            TaskStatus nextStatus = this.taskStatusService.findByStatusOrder(currentOrder - 1);
            task.setStatus(nextStatus);
        }
        return task;
    }
}
