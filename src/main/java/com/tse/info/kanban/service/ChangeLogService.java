package com.tse.info.kanban.service;

import com.tse.info.kanban.domain.ChangeLog;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ChangeLogService {
    List<ChangeLog> findAllChangeLogs();
}
