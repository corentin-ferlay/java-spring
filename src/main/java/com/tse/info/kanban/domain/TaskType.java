package com.tse.info.kanban.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class TaskType {
    private @Id @GeneratedValue Long id;

    private String label;

    @OneToMany(mappedBy = "type")
    private Set<Task> tasks;

    public TaskType(String label) {
        this.label = label;
    }
}
