package com.tse.info.kanban.service;

import com.tse.info.kanban.domain.TaskType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TaskTypeService {

    List<TaskType> findAllTaskTypes();
}
