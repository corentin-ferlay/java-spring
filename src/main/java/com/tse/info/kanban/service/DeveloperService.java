package com.tse.info.kanban.service;

import com.tse.info.kanban.domain.Developer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DeveloperService {
    List<Developer> findAllDevelopers();
}
