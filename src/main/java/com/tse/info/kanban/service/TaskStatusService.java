package com.tse.info.kanban.service;

import com.tse.info.kanban.domain.TaskStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TaskStatusService {

    List<TaskStatus> findAllTaskStatuses();

    TaskStatus findByStatusOrder(Integer statusOrder);
}
