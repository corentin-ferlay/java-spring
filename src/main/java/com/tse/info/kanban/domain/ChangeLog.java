package com.tse.info.kanban.domain;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class ChangeLog {
    private @Id @GeneratedValue Long id;

    private LocalDate occurred;

    @OneToOne
    private TaskStatus sourceStatus;

    @OneToOne
    private TaskStatus targetStatus;

    @ManyToOne
    private Task task;

    public ChangeLog(TaskStatus sourceStatus, TaskStatus targetStatus) {
        this.sourceStatus = sourceStatus;
        this.targetStatus = targetStatus;
        this.occurred = LocalDate.now();
    }
}
