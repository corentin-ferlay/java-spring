package com.tse.info.kanban.service;

import com.tse.info.kanban.domain.Task;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public interface TaskService {
    Collection<Task> findAllTasks();
    Task findTask(Long id);
    Task moveRightTask(Task task);
    Task moveLeftTask(Task task);
}
