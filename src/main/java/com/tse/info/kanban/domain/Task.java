package com.tse.info.kanban.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Task {
    private @Id @GeneratedValue Long id;
    private String title;

    @NotNull(message = "Number of hours forecast cannot be null")
    private Integer nbHoursForecast;

    @NotNull(message = "Number of hours real cannot be null")
    private Integer nbHoursReal;
    private LocalDate created;

    @ManyToOne
    private TaskType type;

    @ManyToOne
    private TaskStatus status;

    @ManyToMany(fetch = FetchType.EAGER)
    @EqualsAndHashCode.Exclude
    @JsonIgnoreProperties({"password", "startContract", "tasks"})
    private Set<Developer> developers;

    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
    private Set<ChangeLog> logs;

    public Task() {
        this.developers = new HashSet<>();
        this.logs = new HashSet<>();
    }

    public Task(String title, Integer nbHoursForecast, TaskType taskType, TaskStatus taskStatus) {
        this.developers = new HashSet<>();
        this.logs = new HashSet<>();
        this.title = title;
        this.nbHoursForecast = nbHoursForecast;
        this.type = taskType;
        this.created = LocalDate.now();
        this.status = taskStatus;
    }

    public void addDeveloper(Developer developer) {
        developer.getTasks().add(this);
        this.developers.add(developer);
    }
}
